#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <numeric>
using namespace std;

struct AffineCipher {
    int key_a;
    int key_b;
    string input;
    string output;
    string decrypt_out;
    string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
    
    void crypt() {
        assert( gcd( key_a, alphabet.length() ) == 1 );

        for( auto item: input ) {
            int current_position = ( key_a * alphabet.find( item ) + key_b ) % alphabet.length();
            output.push_back( alphabet[ current_position ] );
        }
    }
    void decrypt() {
        assert( input.size() && input.size() == output.size() );

        for( auto item: output ) {
            int pos = alphabet.find( item );
            int temp_pos = pos - key_b;
            int cur_pos = ( helper( key_a ) * ( temp_pos < 0 ? temp_pos + alphabet.length() : temp_pos ) ) % alphabet.length();
            decrypt_out.push_back( alphabet[ cur_pos ] );
        }

        check_decrypt();
    }

private:
    void check_decrypt() {
        assert( input == decrypt_out );
    }
    int helper( int a ) {
        for(int index = 0; index < alphabet.length(); ++index){
            if( ( index * a ) % alphabet.length() == 1)
                return index;
        }
    }
};

int main( int argc, char* argv[] ){
    AffineCipher ac;
    ifstream sin( "key.txt" );
    assert( sin.is_open() );
    string buffer;
    sin >> ac.key_a;
    assert( ac.key_a > 0 );

    sin >> ac.key_b;
    assert( ac.key_b > 0 && ac.key_b < ac.alphabet.length() );

    sin.close();

    sin.open( "in.txt" );
    assert( sin.is_open() );
    getline( sin, buffer );
    assert( buffer.size() );
    for( auto item: buffer ) {
        assert( ac.alphabet.find( item ) != string::npos );
        ac.input.push_back( item );
    }
    sin.close();

    cout << "Start crypting..." << endl;

    ac.crypt();

    ofstream sout( "crypt.txt" );
    assert( sout.is_open() );
    sout << ac.output << endl;
    sout.close();

    cout << "Start decrypting..." << endl;

    ac.decrypt();

    sout.open( "decrypt.txt" );
    assert( sout.is_open() );
    sout << ac.decrypt_out << endl;
    sout.close();

    cout << "Ready!" << endl;
}