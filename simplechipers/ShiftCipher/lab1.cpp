#include <iostream>
#include <fstream>
#include <string>
using namespace std;

struct ShiftCipher{
    string key;
    string input;
    string output;
    string decrypt_out;
    string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";

    void crypt() {
        assert( key.size() && input.size()  );
        int pos = alphabet.find( key ) + 1;
        for( auto item: input ) {
            int temp_pos = alphabet.find( item ) + pos;
            if ( temp_pos >= alphabet.size() ) {
                temp_pos %= alphabet.size();
            }
            output.push_back( alphabet[ temp_pos ] );
        }
    }
    void decrypt() {
        assert( key.size() && input.size() && output.size() );
        int pos = alphabet.find( key ) + 1;
        for( auto item: output ) {
            auto temp = alphabet.find( item );
            int temp_pos = alphabet.find( item ) - pos;
            if ( temp_pos < 0 ) {
                temp_pos += alphabet.size();
            }
            decrypt_out.push_back( alphabet[ temp_pos ] );
        }
        check_decrypt();
    }

private:
    void check_decrypt() {
        assert( input == decrypt_out );
    }
};
int main( int argc, char* argv[] ){
    ShiftCipher sc;
    string buffer;
    ifstream sin( "key.txt" );
    assert( sin.is_open() );
    getline( sin, buffer );
    assert( buffer.size() );
    assert( sc.alphabet.find( buffer[0] ) != string::npos );
    sc.key.push_back( buffer[0] );
    sin.close();

    sin.open( "in.txt" );
    assert( sin.is_open() );
    getline( sin, buffer );
    assert( buffer.size() );
    for( auto item: buffer ) {
        assert( sc.alphabet.find( item ) != string::npos );
        sc.input.push_back( item );
    }
    sin.close();

    cout << "Start crypting..." << endl;

    sc.crypt();

    ofstream sout( "crypt.txt" );
    assert( sout.is_open() );
    sout << sc.output << endl;
    sout.close();

    cout << "Start decrypting..." << endl;

    sc.decrypt();

    sout.open( "decrypt.txt" );
    assert( sout.is_open() );
    sout << sc.decrypt_out << endl;
    sout.close();

    cout << "Ready!" << endl;
}