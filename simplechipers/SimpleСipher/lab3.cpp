#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <set>
using namespace std;

struct SimpleСipher {
    string key;
    string input;
    string output;
    string decrypt_out;
    string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
    
    void crypt() {
        assert( key.size() && input.size() );
        for( auto item: input ) {
            output.push_back( key[ find_index_inc( item ) ] );
        }
    }
    void decrypt() {
        assert( key.size() && input.size() && output.size() == input.size() );
        for( auto item: output ) {
            decrypt_out.push_back( alphabet[ find_index_dec( item ) ] );
        }
        check_decrypt();
    }

private:
    size_t find_index_inc( char ch ) {
        return alphabet.find( ch );
    }
    size_t find_index_dec( char ch ) {
        return key.find( ch );
    }
    void check_decrypt() {
        assert( input == decrypt_out );
    }
};
int main(){
    SimpleСipher sc;
    ifstream sin( "key.txt" );
    string buffer;
    getline( sin, buffer );
    assert( buffer.size() );
    for( auto item: buffer ) {
        assert( sc.alphabet.find( item ) != string::npos );
        sc.key.push_back( item );
    }
    set<char> myset(sc.key.begin(), sc.key.end());
    assert( myset.size() == sc.key.length() );
    sin.close();

    sin.open( "in.txt" );
    assert( sin.is_open() );
    getline( sin, buffer );
    assert( buffer.size() );
    for( auto item: buffer ) {
        assert( sc.alphabet.find( item ) != string::npos );
        sc.input.push_back( item );
    }
    sin.close();

    cout << "Start crypting..." << endl;

    sc.crypt();

    ofstream sout( "crypt.txt" );
    sout << sc.output << endl;
    sout.close();

    cout << "Start decrypting..." << endl;

    sc.decrypt();

    sout.open( "decrypt.txt" );
    sout << sc.decrypt_out << endl;
    sout.close();

    cout << "Ready!" << endl;
}