#include <iostream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <cassert>
#include <fstream>

using namespace std;

// we use unsigned int for binary representation of poly, but we can change in any type of unsgined number
typedef unsigned int poltype;

struct Polynomial {
    static poltype irreducible_pol;
    poltype degrees;

    Polynomial(): degrees(0){}

    Polynomial( poltype _degrees ): degrees( _degrees ){}

    Polynomial( const Polynomial& p ): degrees( p.degrees ) {}

    // + are equal to - in this field
    Polynomial operator+( const Polynomial& p ) const {
        return Polynomial( this->degrees ^ p.degrees );
    }

    // first we multiply polynomials, than we find remainder in div by irreducible polynomial
    Polynomial operator*( const Polynomial& p ) const {
        if( p.degrees == 0 || this->degrees == 0)
            return Polynomial( 0 );
        poltype res = getMult( this->degrees, p.degrees );
        res = getRemainder( res, irreducible_pol ).first;

        return Polynomial( res );
    }

    // integer division
    Polynomial operator/ ( const Polynomial& p ) const {
        assert( p.degrees != 0 );
        return Polynomial( getDiv( p.degrees ) );
    }

    // find remainder
    Polynomial operator% ( const Polynomial& p ) const {
        assert( p.degrees != 0 );
        return Polynomial( getRemainder( this->degrees, p.degrees ).first );
    }

    // pow polynomial into degree
    Polynomial powpol( unsigned int deg ) const {
        if( deg == 0 )
            return Polynomial();
        Polynomial new_p( *this );
        for( unsigned int i = 1; i < deg; ++i ) {
            new_p = new_p * (*this);
        }
        return new_p;
    }

    // find reverse element
    Polynomial reverseElement() const {
        return powpol( static_cast<int> ( pow( 2, Polynomial( irreducible_pol ).getDegree() - 1 ) ) - 2 );
    }

    friend ostream& operator<<( ostream& os, const Polynomial& p ) {
        poltype output_degrees = p.degrees;
        string xpol;
        poltype counter = 0;
        os << " Binary representation = ";
        while( output_degrees != 0 ) {
            bool flag = ( output_degrees & 0b1 );
            os << flag;
            output_degrees >>= 1;
            if( flag ) {
                if( !counter || !xpol.length() ) {
                    xpol += p.printHelper( counter );
                } else {
                    xpol += " + " + p.printHelper( counter );
                }
            }
            ++counter;
        }
        os << endl;
        os << " Standart representation = " << xpol;

        return os;
    }

    friend istream& operator>>( istream& is, Polynomial& p ) {
        string str;
        is >> str;
        poltype degree_of_pol = str.length();
        assert( degree_of_pol <= sizeof( poltype ) * 8 );
        poltype degrees = 0, counter = 0;
        for( poltype index = 0; index < degree_of_pol ; ++index ) {
            poltype tmp_degree = 0;
            char item = str.at( index );
            assert( item == '0' || item == '1' );
            if( item == '1' ) {
                tmp_degree = 1;
                tmp_degree <<= counter;
                degrees |= tmp_degree;
            }
            ++counter;
        }
        p.degrees = degrees;
        return is;
    }

private:

    string printHelper( poltype deg ) const {
        switch( deg ){
            case 0:
                return "1";
            case 1:
                return "x";
            default:
                return "x^" + to_string( deg );
        }
    }

    poltype getDiv( poltype b ) const {
        return getRemainder( this->degrees, b ).second;
    }

    pair< poltype, poltype > getRemainder( poltype a, poltype b ) const {
        poltype division = 0;
        int deg = countNextShift( a, b );
        while( deg >= 0 ) {
            division |= 0b1 << deg;
            poltype temp_irr = b << deg;
            a ^= temp_irr;
            --deg; // usless
            if( Polynomial( a ).getDegree() < Polynomial( b ).getDegree() )
                break;
            deg = countNextShift( a, b );
        }
        return make_pair( a, division );
    }

    poltype countNextShift( poltype a, poltype b ) const {
        return Polynomial( a ).getDegree() - Polynomial( b ).getDegree();
    }

    poltype getDegree() const {
        poltype a = this->degrees;
        poltype counter = 0;
        while ( a != 0) {
            a >>= 1;
            ++counter;
        }
        return counter;
    }

    poltype getMult( poltype a, poltype b) const {
        auto vec = getVector( b );
        assert( vec.size() );
        vector< poltype > vector_before_xor;
        poltype res = 0;
        for( auto item: vec ) {
            vector_before_xor.push_back( a << item );
        }
        for( auto item: vector_before_xor ) {
            res ^= item;
        }
        return res;
    }

    // helper for multiply
    vector< poltype > getVector( poltype a ) const {
        vector< poltype > vec;
        poltype counter = 0;
        while( a != 0 ) {
            if ( a & 0b1 ) {
                vec.push_back( counter );
            }
            ++counter;
            a >>= 1;
        }
        return vec;
    }
};
poltype Polynomial::irreducible_pol;

int unit_test() {
    cout << " Unit test running! " << endl;

    Polynomial::irreducible_pol = 0b1011;
    Polynomial p1( 0b101011001 ), p2( 0b1110 );
    assert( ( p1 % p2 ).degrees == 0b111 );
    assert( ( p1 / p2 ).degrees == 0b111001 );
    assert( ( p1 * p1.reverseElement() ).degrees == 0b1 );
    assert( Polynomial( 0b101 ).powpol( 3 ).degrees == 0b110 );
    assert( ( Polynomial( 0b111 ) * Polynomial( 0b101 ) ).degrees == 0b110 );

    cout << " Unit test has passed successed! " << endl;

    return 0;
}

void outputBinOp( Polynomial p1, Polynomial p2, char op ) {
    cout << " Polynomial one: " << endl << p1 << endl;
    cout << " Polynomial two: " << endl << p2 << endl;
    cout << " Operation " << op << endl;
}
void outputUnOp( Polynomial p, char op ) {
    cout << " Polynomial: " << endl << p << endl;
    cout << " Operation " << op << endl;
}
void outputReverse( Polynomial p ) {
    cout << " Polynomial: " << endl << p << endl;
    cout << " Operation ^-1 "<< endl;
}

int main( int argc, char* argv[] ) {
    if( argc > 1 ) {
        return unit_test();
    }

    ifstream sin( "polynom.txt" );
    assert( sin.is_open() );
    Polynomial irr_pol;
    sin >> irr_pol;

    Polynomial::irreducible_pol = irr_pol.degrees;
    cout << " Irreducible polynom :" << Polynomial( Polynomial::irreducible_pol ) << endl << endl;

    sin.close();
    sin.open( "input.txt" );
    assert( sin.is_open() );

    ofstream sout( "output.txt" );
    assert( sout.is_open() );

    Polynomial p1, p2;
    sin >> p1;
    string op;
    sin >> op;
    switch( op[0] ) {
        case '+':
        case '-':
            sin >> p2;
            outputBinOp( p1, p2, op[0] );
            cout << p1 + p2 << endl;

            sout << p1 + p2 << endl;

            break;
        case '*':
            sin >> p2;
            outputBinOp( p1, p2, op[0] );
            cout << p1 * p2 << endl;

            sout << p1 * p2 << endl;
            
            break;
        case '/':
            sin >> p2;
            outputBinOp( p1, p2, op[0] );
            cout << p1 / p2 << endl;
            cout << " Remainder: " << p1 % p2 << endl;

            sout << p1 / p2 << endl;
            sout << " Remainder: " << p1 % p2 << endl;

            break;
        case '^':
            int deg;
            sin >> deg;
            if ( deg < 0 ) {
                outputReverse( p1 );
                assert( ( p1 * p1.reverseElement() ).degrees == 0b1 );
                cout << p1.reverseElement() << endl;

                sout << p1.reverseElement() << endl;
            
            } else {
                outputUnOp( p1, op[0] );
                cout << p1.powpol( deg ) << endl;

                sout << p1.powpol( deg ) << endl;
            
            }
            break;
        default:
            assert( false );
            break;
    }

    return 0;
}
